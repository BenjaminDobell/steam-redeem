export const STATUS = Object.freeze({
    FAILED: -1,
    DEFAULT: 1,
    REDEEMING: 2,
    REDEEMED: 3
})

export const EVENT_NAME = 'au.com.glassechidna.ge-steam-redeem.redemption'
