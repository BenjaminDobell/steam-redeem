import { STATUS as REDEMPTION_STATUS } from './redemption'

import { redeem } from './steam'

import Library from './library'

const library = new Library(browser.storage)

async function redeemItem(tabId, title, key) {
    try {
        const result = await redeem(key)

        if (result.success === 1) {
            browser.tabs.sendMessage(tabId, {redeemed: {key, status: REDEMPTION_STATUS.REDEEMED}})
        } else {
            if (result.purchase_receipt_info && result.purchase_receipt_info.line_items && result.purchase_receipt_info.line_items.length > 0) {
                browser.tabs.sendMessage(tabId, {redeemed: {key, status: REDEMPTION_STATUS.REDEEMED}})
                alert("You've already redeemed '" + title + "'")
            } else {
                browser.tabs.sendMessage(tabId, {redeemed: {key, status: REDEMPTION_STATUS.FAILED}})

                if (result.purchase_result_details === 53) {
                    alert("Steam reported too many key redemption attempts whilst redeeming '" + title + "'. You'll have to try again later.")
                } else {
                    alert("Invalid key encountered whilst redeeming '" + title + "'")
                }
            }
        }
    } catch (e) {
        browser.tabs.sendMessage(tabId, {redeemed: {key, status: REDEMPTION_STATUS.DEFAULT}})
        alert("You must first login to Steam through your browser before you can redeem keys using Steam Redeem.")
        browser.tabs.create({ url: 'https://store.steampowered.com/login/?redir=' })
    }
}

async function redeemItems(tabId, items) {
    for (const item of items) {
        const {title, key} = item

        if (title && key) {
            await redeemItem(tabId, title, key)
        }
    }
}

function postRedeemAll(tab) {
    browser.tabs.sendMessage(tab.id, {redeemAll: true})
}

browser.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.keysFound !== undefined) {
        if (request.keysFound === true) {
            browser.pageAction.show(sender.tab.id)

            if (!browser.pageAction.onClicked.hasListener(postRedeemAll)) {
                browser.pageAction.onClicked.addListener(postRedeemAll)
            }
        } else {
            browser.pageAction.hide(sender.tab.id)
            browser.pageAction.onClicked.removeListener(postRedeemAll)
        }
    }

    if (request.redeem !== undefined) {
        redeemItems(sender.tab.id, request.redeem)
    }
})
