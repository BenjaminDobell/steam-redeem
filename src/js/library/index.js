const SET = 'set'
const GET = 'get'

export default class Library {
	#storage = null
	#loadPromise = null

	#licenses = null

	#getLicense = async (key) => {
		if (!this.#licenses) {
			await this.load()
		}

		return this.#licenses[key]
	}

	#setLicense = async (key, license) => {
		if (!this.#licenses) {
			await this.load()
		}

		this.#licenses[key] = license

		await this.#storage.set({[key]: license})
	}

	#onMessage = async (request, sender, sendResponse) => {
		if (request.method === SET) {
			await this.#setLicense(request.key, request.license)
		} else if (request.method === GET) {
			return await this.#getLicense(request.key)
		}
	}

	constructor(storage) {
		this.#storage = storage

		browser.runtime.onMessage.addListener(this.#onMessage)
	}

	async load() {
		if (!this.#loadPromise) {
			this.#loadPromise = async () => {
				this.#loadPromise = null
				this.#licenses = await this.#storage.get()
			}
		}

		return this.#loadPromise
	}

	static async getLicense(runtime, key) {
		return runtime.sendMessage({method: GET, key})
	}

	static async setLicense(runtime, key, license) {
		return runtime.sendMessage({method: SET, key, license})
	}
}
