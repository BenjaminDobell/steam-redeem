import { insertRedeemButton, modifyRedeemButton } from './manipulators'

import observe from './observe'

const KEYS_KEY_CONTAINER_PATH = "//*[./*[@class='game-name'] and .//*[contains(@class, 'steam-redeem-button')]]"
const LIBRARY_KEY_CONTAINER_PATH = "//*[contains(@class, 'platform') and contains(@class, 'steam')]//*[contains(@class, 'key-redeemer')]"
const DOWNLOADS_KEY_CONTAINER_PATH = "//*[contains(@class, 'key-redeemer') and .//*[contains(@class, 'steam-redeem-button')]]"

const KEY_CONTAINER_PATH = [
	KEYS_KEY_CONTAINER_PATH,
	LIBRARY_KEY_CONTAINER_PATH,
	DOWNLOADS_KEY_CONTAINER_PATH,
].join('|')

function detectTitle(containerNode) {
	const gameNameNode = document.evaluate(".//*[@class='game-name']", containerNode, null, XPathResult.ANY_TYPE, null).iterateNext()

	if (gameNameNode) {
		return gameNameNode.textContent.trim().split('\n')[0]
	}

	const headingTextNode = document.evaluate(".//*[@class='heading-text']", containerNode, null, XPathResult.ANY_TYPE, null).iterateNext()

	if (headingTextNode) {
		return headingTextNode.textContent.trim()
	}

	return null
}

function detectRedeemButtonNode(containerNode) {
	return document.evaluate(".//*[@class='steam-redeem-button']", containerNode, null, XPathResult.ANY_TYPE, null).iterateNext()
}

function detectKeyNode(containerNode) {
	return document.evaluate(".//*[contains(concat(' ', normalize-space(@class), ' '), ' redeemed ')]/*[contains(@class, 'keyfield-value')]", containerNode, null, XPathResult.ANY_TYPE, null).iterateNext()
}

function detectKeys() {
	const redeemables = []
	const containerNodes = document.evaluate(KEY_CONTAINER_PATH, document, null, XPathResult.ANY_TYPE, null)

	let containerNode = containerNodes.iterateNext()

	while (containerNode) {
		const keyNode = detectKeyNode(containerNode)
		const title = detectTitle(containerNode)

		if (keyNode && title) {
			const key = keyNode.textContent.trim()
			const buttonNode = detectRedeemButtonNode(containerNode)

			const dom = buttonNode ? {
				manipulate: modifyRedeemButton,
				node: buttonNode,
			} : {
				manipulate: insertRedeemButton,
				node: keyNode.parentNode,
			}

			redeemables.push({key, title, dom})
		}

		containerNode = containerNodes.iterateNext()
	}

	return redeemables
}

const hostnameRegex = /^(www\.)?humblebundle\.com/
let isFirstRun = true

export default () => {
	if (hostnameRegex.test(window.location.hostname)) {
		if (isFirstRun) {
			isFirstRun = false
			observe()
		}

		return detectKeys()
	}

	return []
}
