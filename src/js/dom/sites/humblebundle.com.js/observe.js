import { update } from '../..'

const MAIN_CONTENT = "//*[contains(concat(' ', normalize-space(@class), ' '), ' inner-main-wrapper ')]"

const observer = new MutationObserver(() => {
	observer.takeRecords()
	update()
})

export default () => {
	observer.disconnect()

	const containerNodes = document.evaluate(MAIN_CONTENT,document, null, XPathResult.ANY_TYPE, null)

	let containerNode = containerNodes.iterateNext()

	while (containerNode) {
		observer.observe(containerNode, {subtree: true, attributes: true, childList: true})
		containerNode = containerNodes.iterateNext()
	}
}
