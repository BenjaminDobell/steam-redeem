import { EVENT_NAME as REDEMPTION_EVENT, STATUS as REDEMPTION_STATUS } from '../../../redemption'
import Library from '../../../library/index'

function updateStatus(redeemable, status) {
	const {dom: {node}} = redeemable

	if (status != node.getAttribute('data-ge-steam-redeem-status')) {
		node.setAttribute('data-ge-steam-redeem-status', status)

		switch (status) {
			default:
			case REDEMPTION_STATUS.DEFAULT:
				node.innerHTML = 'Redeem'
				break

			case REDEMPTION_STATUS.REDEEMING:
				node.innerHTML = "<i class='hb hb-spinner hb-spin'>"
				break

			case REDEMPTION_STATUS.REDEEMED:
				node.innerHTML = "<i class='hb hb-check'></i>"
				break
		}
	}
}

function setupRedeemButton(redeemable, button) {
	const {key, title} = redeemable

	button.addEventListener('click', (e) => {
		const status = button.getAttribute('data-ge-steam-redeem-status') || 0

		if (title && key && status <= REDEMPTION_STATUS.DEFAULT) {
			updateStatus(redeemable, REDEMPTION_STATUS.REDEEMING)
			browser.runtime.sendMessage({redeem: [{title, key}]})
		}

		e.preventDefault()
	})

	button.addEventListener(REDEMPTION_EVENT, (e) => {
		updateStatus(redeemable, e.detail.status)
	})

	Library.getLicense(browser.runtime, key).then(license => {
		if (license) {
			updateStatus(redeemable, REDEMPTION_STATUS.REDEEMED)
		}
	})

	updateStatus(redeemable, REDEMPTION_STATUS.DEFAULT)
}

export function modifyRedeemButton(redeemable) {
	const {key, title, dom: {node: button}} = redeemable

	if (!button.classList.contains('ge-steam-redeem-large-button')) {
		button.classList.add('ge-steam-redeem-large-button')

		button.setAttribute('data-ge-steam-redeem-key', key)

		button.style.backgroundColor = '#000'
		button.style.color = '#fff'

		setupRedeemButton(redeemable, button)
	}
}

export function insertRedeemButton(redeemable) {
	const {key, title, dom: {node}} = redeemable

	let button = document.evaluate("./*[contains(@class, 'ge-steam-redeem-large-button')]", node, null, XPathResult.ANY_TYPE, null).iterateNext()

	if (!button) {
		button = document.createElement('a')

		button.classList.add('ge-steam-redeem-large-button')

		button.setAttribute('data-ge-steam-redeem-key', key)

		button.style.display = 'flex'
		button.style.flexDirection = 'column'
		button.style.alignItems = 'center'
		button.style.justifyContent = 'center'
		button.style.textDecoration = 'node'
		button.style.position = 'absolute'
		button.style.margin = 'auto'
		button.style.borderRadius = '3px'
		button.style.right = '3%'
		button.style.top = '0'
		button.style.bottom = '0'
		button.style.height = '30px'
		button.style.width = '60px'
		button.style.backgroundColor = '#000'
		button.style.color = '#fff'

		setupRedeemButton(redeemable, button)

		node.appendChild(button)
	}
}
