import sites from './sites'

function detectRedeemables() {
	const redeemables = []

	for (const detect of sites) {
		redeemables.push(...detect())
	}

	return redeemables
}

function manipulateDom(redeemables) {
	for (const redeemable of redeemables) {
		if (redeemable.dom) {
			redeemable.dom.manipulate(redeemable)
		}
	}
}

export function update() {
	const redeemables = detectRedeemables()
	manipulateDom(redeemables)
	browser.runtime.sendMessage({keysFound: redeemables.length > 0})
	return redeemables
}
