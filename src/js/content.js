import { EVENT_NAME as REDEMPTION_EVENT } from './redemption'

import { update } from './dom'

browser.runtime.onMessage.addListener(function(request, sender, sendResponse) {
	if (request.redeemAll) {
		const redeemables = update()
		browser.runtime.sendMessage({redeem: redeemables.map(entry => ({title: entry.title, key: entry.key}))})
	}

	if (request.redeemed) {
		const {key, status} = request.redeemed
		const node = document.evaluate(`.//*[@data-ge-steam-redeem-key='${key}']`, document, null, XPathResult.ANY_TYPE, null).iterateNext()

		if (node) {
			node.dispatchEvent(new CustomEvent(REDEMPTION_EVENT, {detail: {key, status}}))
		}
	}
})

update()
