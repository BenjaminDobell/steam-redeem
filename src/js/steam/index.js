const REDEEM_URL = 'https://store.steampowered.com/account/ajaxregisterkey/'
const USER_DATA_URL = 'https://store.steampowered.com/dynamicstore/userdata/'
const GET_APP_LIST = 'http://api.steampowered.com/ISteamApps/GetAppList/v0002/'

let sessionId = null

export async function fetchSessionId() {
	if (!sessionId) {
		const cookie = await browser.cookies.get({
			url: 'https://store.steampowered.com',
			name: 'sessionid'
		})

		sessionId = cookie.value
	}

	return sessionId
}

export async function redeem(key) {
	const sessionId = await fetchSessionId()

	const urlEncoding = new URLSearchParams()
	urlEncoding.append('product_key', key)
	urlEncoding.append('sessionid', sessionId)

	const response = await fetch(REDEEM_URL, {
		method: 'POST',
		headers: {
			'Accept': 'application/json',
		},
		body: urlEncoding,
		credentials: 'include'
	})

	return await response.json()
}
